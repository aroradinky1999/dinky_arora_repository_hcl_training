package com.greatlearning.assignment.week2;

public class IllegalArgumentExceptions extends Exception  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IllegalArgumentExceptions() {
		super();
		// TODO Auto-generated constructor stub
	}

	public IllegalArgumentExceptions(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public IllegalArgumentExceptions(String s) {
		super(s);
		// TODO Auto-generated constructor stub
	}

	public IllegalArgumentExceptions(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
