package com.greatlearning.assignment.week2;

import java.util.ArrayList;


public class Main {

	public static void main(String[] args) {
		System.out.println("List of Employees: ");
		System.out.println();
		System.out.println("S.No. "+ " Name   "  + "Age " +  " Salary(INR)   "+
		"Department "+ " Location ");
		System.out.println();
		ArrayList<Employee> employees = new ArrayList<>();
		try {
		 Employee e1 = new Employee(1, "Aman    " ,   20,  1100000,   "   IT",  "Delhi");
		 
         Employee e2 = new Employee(2, "Bobby   ",  22,  500000, "    Hr", "Bombay");
         
         Employee e3 = new Employee(3, "Zoe     ",    20,  750000, "  Admin", "Delhi");
        
         Employee e4 = new Employee(4, "Smitha  ",21,  1000000, "   IT", "Chennai");
         
         Employee e5 = new Employee(5, "Smitha  ",24,  1200000, "   Hr", "Bengaluru");
		
        // Adding employee details
         employees.add(e1);
         employees.add(e2);
         employees.add(e3);
         employees.add(e4);
         employees.add(e5);
         // showing in form of table
         System.out.println(e1);
         System.out.println(e2);
         System.out.println(e3);
         System.out.println(e4);
         System.out.println(e5);
         System.out.println();
         
		}catch(IllegalArgumentExceptions illegalArgumentException) {
			System.out.println("Please enter valid details");
			illegalArgumentException.printStackTrace();
		}
		
		System.out.println();
	
         //Names of employee in sorted order
         System.out.println("* Names of all employees in the sorted order are:");
         
        DataStructureA dsa = new DataStructureA();
        dsa.sortingNames(employees);      
      
         System.out.println();
         System.out.println();
         //count of city
         DataStructureB dsb = new DataStructureB();
         System.out.println("* Count of Employees from each city:");
         dsb.cityNameCount(employees);
              System.out.println();
              System.out.println();
              //salary per month
          System.out.println("* Monthly Salary of employee along with their ID is:");    
           dsb.monthlySalary(employees);
           
         
   		}

       



      
         
	}


