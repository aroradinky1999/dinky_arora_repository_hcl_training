package com.greatlearning.assignment.week2;

public class Employee {
	

	private int id;
	private String name;
	private int age;
	private int salary;     // per annum
	private String department;
	private String city;
	
	 //empty constructor
	
	public Employee() {         
		super();
		
	}
	 //parameterized constructor
	
	public Employee(int id, String name, int age, int salary, String department, String city) throws IllegalArgumentExceptions  {
		super();                              
		 if(id<0)

         {

                    throw new IllegalArgumentExceptions("Invalid Id : Id cannot be 0 or less");

         }

         this.id = id;

         if(name.isEmpty()|| name == null) {

                    throw new IllegalArgumentExceptions("Invalid Name : Name cannot be null");
         }

         this.name = name;

         if(age<=0)
         {
                    throw new IllegalArgumentExceptions("Invalid Age : Age cannot be zero or less");
         }

         this.age = age;

         if(salary<0)
         {
                    throw new IllegalArgumentExceptions("Invalid Salary : Salary cannot be zero or less");
         }

         this.salary = salary;

         if( department.isEmpty()||department == null) {

                    throw new IllegalArgumentExceptions("Invalid Department : Department cannot be null");

         }

         this.department = department;

         if(city.isEmpty()|| city==null)

         {
                    throw new IllegalArgumentExceptions("Invalid City : City cannot be null");

         }

         this.city = city;

        

}




	
	
	//getter and setter method
	
	public int getId() {                      
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
  //ToString Method
	@Override
	public String toString() {
		return  id +  "   " + name + "   " + age + "    " + salary + "      "
				+ department + "     " + city;
	}

	   
}
	

	

	
	


