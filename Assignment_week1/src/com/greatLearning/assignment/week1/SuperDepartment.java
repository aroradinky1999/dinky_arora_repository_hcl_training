package com.greatLearning.assignment.week1;

public class SuperDepartment {
	
       // method departmentName of return type string
	    public String departmentName() {
		return "Super Department ";
		}
		// method getTodaysWork of return type string
		public String getTodaysWork() {
		return " No work as of now " ;
		}
		// method getWorkDeadline of return type string
		public String getWorkDeadline() {
		return " Nil ";
		}
		// method isTodayAHoliday of type string 
		public String isTodayAHoliday () {
		return "Today is not a holiday";
		}
		@Override
		public String toString() {
			return "SUPER DEPARTMENT : \n"
					+ "* departmentName = " + departmentName() + "\n"
		            + "* getTodaysWork = " + getTodaysWork()
					+ "\n* getWorkDeadline = " + getWorkDeadline() +
					"\n* isTodayAHoliday = " + isTodayAHoliday() ;
		}
		
}

