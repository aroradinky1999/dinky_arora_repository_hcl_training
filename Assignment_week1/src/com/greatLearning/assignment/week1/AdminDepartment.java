package com.greatLearning.assignment.week1;

public class AdminDepartment extends SuperDepartment {
	
	 // method departmentName of return type string
    public String departmentName() {
    	
	       return "Admin Department ";
	}
	// method getTodaysWork of return type string
	public String getTodaysWork() {
		
	        return "Complete your documents Submission ";
	}
	// method getWorkDeadline of return type string
	public String getWorkDeadline() {
		
	        return "Complete by EOD ";
	}
	// method isTodayAHoliday of type string 
	public String isTodayAHoliday() {
		
	        return super.isTodayAHoliday();
	}
	@Override
	public String toString() {
		return "ADMIN DEPARTMENT : \n"
				+ "* departmentName = " + departmentName() + "\n"
	            + "* getTodaysWork = " + getTodaysWork()
				+ "\n* getWorkDeadline = " + getWorkDeadline() +
				"\n* isTodayAHoliday = " + isTodayAHoliday() ;
	}
	
	
	

}
