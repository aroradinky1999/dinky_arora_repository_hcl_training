package com.greatLearning.assignment.week1;

public class HrDepartment extends SuperDepartment {
	
	 // method departmentName of return type string
    public String departmentName() {
    	
	         return " HR Department ";
	}
	// method getTodaysWork of return type string
	public String getTodaysWork() {
		
	          return " Fill todays worksheet and mark your attendance  ";
	}
	// method  doActivity of type string 
    public String  doActivity() {
    	
		      return " Team Lunch ";
		}
	// method getWorkDeadline of return type string
	public String getWorkDeadline() {
		
	           return "Complete by EOD ";
	}
	// method isTodayAHoliday of type string 
	public String isTodayAHoliday() {
			
		        return super.isTodayAHoliday();
	}
	@Override
	public String toString() {
		return "HR DEPARTMENT : \n"
				+ "* departmentName = " + departmentName() + 
				"\n* getTodaysWork = " + getTodaysWork()
				+ "\n* doActivity = " + doActivity() + 
				"\n* getWorkDeadline = " + getWorkDeadline() + 
				"\n* isTodayAHoliday = "
				+ isTodayAHoliday()  ;
	}
	
	

}
