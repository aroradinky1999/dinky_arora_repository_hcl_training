package com.greatLearning.assignment.week1;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		System.out.println("Select which department details you want : ");
		System.out.println(" 1.Admin Department\n "
		+ "2.Hr Department\n"+ " 3.Tech Department\n"+ " 4.Super Department");
		
		Scanner sc=new Scanner(System.in);
		int Depatments =sc.nextInt();
		
		switch(Depatments)
		{
		case 1:
		
		//  object of AdminDepartment 
		AdminDepartment ad = new AdminDepartment();
		System.out.println(ad);
		break;
		
		case 2:
			
		//  object of HrDepartment 
		HrDepartment hd = new HrDepartment();
		System.out.println(hd);
		break;
		
		case 3:
			
		//  object of TechDepartment 
	    TechDepartment td = new TechDepartment();
		System.out.println(td);
		break;
		
		case 4:
		//  object of SuperDepartment 
		    SuperDepartment sd = new SuperDepartment();
			System.out.println(sd);
			break;
		
		default :
			System.out.println("Please choose right option i.e 1,2,3,4");

	}

	}
}
