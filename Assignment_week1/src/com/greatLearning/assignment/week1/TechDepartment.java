package com.greatLearning.assignment.week1;

public class TechDepartment extends SuperDepartment {
	
	 // method departmentName of return type string
    public String departmentName() {
    	
	           return "Tech Department ";
	}
	// method getTodaysWork of return type string
	public String getTodaysWork() {
		
	            return " Complete coding of module 1";
	}
	// method  getTechStackInformation  of return type string
	public String  getTechStackInformation() {
		
	            return " core Java " ;
	}
	// method  getWorkDeadline of type string 
	public String  getWorkDeadline() {
		
	            return " Complete by EOD ";
	}
	// method isTodayAHoliday of type string 
	public String isTodayAHoliday() {
			
		        return super.isTodayAHoliday();
		}
	@Override
	public String toString() {
		return "TECH DEPARTMENT : \n"
				+ "* departmentName = " + departmentName() + "\n"
				+ "* getTodaysWork = " + getTodaysWork()
				+ "\n* getTechStackInformation = " + getTechStackInformation() +
				"\n* getWorkDeadline = "+ getWorkDeadline() 
				+ "\n* isTodayAHoliday = " + isTodayAHoliday()  ;
	}
	

}
