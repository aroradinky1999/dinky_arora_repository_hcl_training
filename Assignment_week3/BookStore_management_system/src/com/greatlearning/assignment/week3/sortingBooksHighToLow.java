package com.greatlearning.assignment.week3;

import java.util.Comparator;

public class sortingBooksHighToLow implements Comparator<Books> {


	@Override
	public int compare(Books b1, Books b2) {
		return (int)(b2.getPrice()-b1.getPrice());
		
	}

}
