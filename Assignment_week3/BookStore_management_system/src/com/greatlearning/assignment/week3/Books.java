package com.greatlearning.assignment.week3;

public class Books {
	private String name;
	private double price;
	private String genre;
	private int noOfCopiesSold;
	
	

	public Books() {
		super();
		
	}



	public Books(String name, double price, String genre, int noOfCopiesSold)   {
	    super();		   
		this.name = name;
		this.price = price;
		this.genre = genre;
		this.noOfCopiesSold = noOfCopiesSold;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public double getPrice() {
		return price;
	}



	public void setPrice(double price) {
		this.price = price;
	}



	public String getGenre() {
		return genre;
	}



	public void setGenre(String genre) {
		this.genre = genre;
	}



	public int getNoOfCopiesSold() {
		return noOfCopiesSold;
	}



	public void setNoOfCopiesSold(int noOfCopiesSold) {
		this.noOfCopiesSold = noOfCopiesSold;
	}



	@Override
	public String toString() {
		return "Books [name=" + name + ", price=" + price + ", genre=" + genre + ", noOfCopiesSold=" + noOfCopiesSold
				+ "]";
	}
	
	
	


}
