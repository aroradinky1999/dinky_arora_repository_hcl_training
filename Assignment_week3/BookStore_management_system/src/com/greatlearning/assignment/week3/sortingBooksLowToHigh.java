package com.greatlearning.assignment.week3;

import java.util.Comparator;

public class sortingBooksLowToHigh implements Comparator<Books> {

	@Override
	public int compare(Books b1, Books b2) {
		return (int)(b1.getPrice()-b2.getPrice());
	}

}
