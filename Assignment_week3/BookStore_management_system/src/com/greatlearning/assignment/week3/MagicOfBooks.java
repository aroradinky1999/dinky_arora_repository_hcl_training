package com.greatlearning.assignment.week3;

import java.util.HashMap;
import java.util.Map;


public class MagicOfBooks {
		
       //Adding a Book   
	public void Add(HashMap<Integer, Books> books, int key, String bookName, double bookPrice, String bookGenre,int bookNoOfCopy)   {
		 boolean flag=true;
		  for(Object j:books.keySet()) {
			  if(books.containsKey(key)) {
				  flag=false;
				  break;
			  }
		  }
		  if(flag==false) {
			   System.out.println("****Record not added Successfully : Please,Enter unique book Id****");
		  }
		  else {
			   
			   books.put(key, new Books(bookName, bookPrice, bookGenre, bookNoOfCopy));    
			   System.out.println("Record Stored Successfully");
			  
		  }
}
	//Updating a Book
	public void Update(HashMap<Integer, Books> books, int key, String bookName, double bookPrice, String bookGenre,int bookNoOfCopy)  {
		      books.replace(key, new Books(bookName, bookPrice,bookGenre , bookNoOfCopy ));
		      System.out.println(" Record Updated successfully");		
	}
       //Deleting a Book
	public void delete(HashMap<Integer, Books> books, int i) {
		if(books.containsKey(i)) {
			  books.remove(i);
		}
		else {
			  System.out.println("Record not Deleted successfully : Enter Valid Id");
		}
		System.out.println("Record Deleted successfully");
	}

	
        //Displaying Books
	public void display(HashMap<Integer, Books> books) {
		
	       for(Map.Entry<Integer, Books> e : books.entrySet())
	    	   System.out.println("Key = "+ e.getKey() + " , Name = " + e.getValue());

	}
        //Count Of Books
	public void countOfBooks(HashMap<Integer, Books> books) {
		int count=0;
		for(Object i :books.keySet()) {
			count= count +books.get(i).getNoOfCopiesSold();
		}
		System.out.println("Count of Books :  "+count);
	}

	
        //Autobiography Genre Books
	public void autobiographyGenre(HashMap<Integer, Books> books) {
		System.out.println("Books under Autobiography Genre are:");
		boolean flag = true;
		for(Object i:books.keySet()) {
			if((books.get(i).getGenre()).equals("Autobiography")) {
				flag = false;
				System.out.println(books.get(i).getName());
			}
		}
		if(flag==true) {
			System.out.println(" Books  not present under Autobiography genre ");
		}

	}

	

}
