package com.greatlearning.assignment.week3;

import java.util.Comparator;

public class BestSellingBook implements Comparator<Books> {

	@Override
	public int compare(Books b1, Books b2) {
		return b2.getNoOfCopiesSold()-b1.getNoOfCopiesSold();
	}

}
