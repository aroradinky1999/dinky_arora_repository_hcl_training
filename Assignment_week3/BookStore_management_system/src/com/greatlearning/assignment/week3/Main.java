package com.greatlearning.assignment.week3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

	
	public static void main(String[] args) {
		    
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			HashMap<Integer, Books> books = new HashMap<>();
			
			String bookName;
			double bookPrice; 
			String bookGenre;
			int bookNoOfCopy;
		            int key;
	                String con ;
	       
	        
	        	
	        try {
	        	 books.put(1, new Books("Harry", 200 , "Autobiography",1));
	        	 books.put(2, new Books("Walkover" , 400 , "Fiction",2));
	        	 books.put(3, new Books("Revolution" , 300 , "Drama",5));
	        	 books.put(4, new Books("Pollution" , 800 , "Autobiography",4));
	        	 books.put(5, new Books("Nature" , 500 , "Autobiography",2));
	           
	       	 
			    do {
				System.out.println("What do you want to do :\n");
				System.out.println("1:Add a Book \n"+ "2:Update a Book \n"+
			                       "3:Delete a Book \n"+"4:Display a Book \n"+
						           "5:Book Count \n" + "6:Autobiography Books\n"+
						           "7:Books with Price Low to High \n"+
		                           "8:Books with Price High to Low \n"+
		    		               "9:Best Selling Books\n");
				System.out.println();
				System.out.println("Enter your choice:\n");
				MagicOfBooks magicofbooks = new MagicOfBooks();
				
				int ch = sc.nextInt();
				switch(ch) {
				case 1:    
					      
					       
					       System.out.println("Enter the BookID ");
	                       key = sc.nextInt();
	                       System.out.println("Enter the name of book"); 
	                       bookName=sc.next();
					       System.out.println("Enter the price of book"); 
					       bookPrice = sc.nextDouble();
					       System.out.println("Enter the genre of book"); 
					       bookGenre = sc.next();
					       System.out.println("Enter the number of copies sold");
					       bookNoOfCopy = sc.nextInt();
					       magicofbooks.Add(books,key, bookName, bookPrice, bookGenre, bookNoOfCopy);					      
					       break;
				case 2:   
					      
					       System.out.println("Enter the BookID you want to Update");
					       key = sc.nextInt();
					       System.out.println("Enter the name of book"); 
	                       bookName = sc.next();
					       System.out.println("Enter the price of book"); 
					       bookPrice = sc.nextDouble();
					       System.out.println("Enter the genre of book"); 
					       bookGenre = sc.next();
					       System.out.println("Enter the number of copies sold");
					       bookNoOfCopy = sc.nextInt();
					       magicofbooks.Update(books,key,bookName,bookPrice,bookGenre,bookNoOfCopy);
					       break;
					       
				case 3:  
					      
					       System.out.println("Enter the BookId you want to delete");
					       int i = sc.nextInt();
					       magicofbooks.delete(books,i);		       
					       break;
					       
				case 4:    
					       System.out.println("Book details:");
					       magicofbooks.display(books);				       
					       break;
				case 5 :
					       magicofbooks.countOfBooks(books);
					       break;
				case 6:   
					       magicofbooks.autobiographyGenre(books);
					       break;
				case 7:
					       Collection<Books> a = books.values();
			               ArrayList<Books> alb = new ArrayList<>(a);
			               Collections.sort(alb, new sortingBooksLowToHigh());
			               System.out.println("Books with Price Low to High");
			    		   for(Object obj:alb) {
			    		   System.out.println(obj);			    	     
			    		   }
			    	       break;
				case 8: 	       
					       Collection<Books> b = books.values();
			    	       ArrayList<Books> al = new ArrayList<>(b);
			               Collections.sort(al, new sortingBooksHighToLow());
			               System.out.println("Books with Price High to Low");
			    		   for(Object o:al) {
			    			  System.out.println(o);
			    		   }
			    	       break;
			    	       
				case 9:	      
					       Collection<Books> bb = books.values();
	    	               ArrayList<Books> li = new ArrayList<>(bb);
	                       Collections.sort(li, new BestSellingBook());
	                       System.out.println("Best seling Books ");
	    		           for(Object ob:li) {
	    		        	  System.out.println(ob);
	    		           }	    	    
	    	               break;					
					      
			    default:
			    	     System.out.println("Invalid data!");
				
				}
				System.out.println("Do you want to continue type y or n?");
				con = sc.next();
			    } while (con.equalsIgnoreCase("y"));
			   
					    
			    
	        } catch(Exception e) {
		      System.out.println("Please enter valid data :"+e);
	        }           
		   
	          System.out.println("Finish Thank You!");
		    
	}

}
	
