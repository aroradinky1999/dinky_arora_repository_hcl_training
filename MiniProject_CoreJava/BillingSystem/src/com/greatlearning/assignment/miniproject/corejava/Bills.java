package com.greatlearning.assignment.miniproject.corejava;

import java.util.ArrayList;
import java.util.Date;

public class Bills {
	private String name;
	private ArrayList<MenuItems> items;
	private double cost;
	private Date time;
	
	public Bills() {}
	
	public Bills(String name, ArrayList<MenuItems> items, double cost, Date time) throws IllegalArgumentException {
		super();
		
		this.name = name;
		this.items = items;
		if(cost<0)
		{
			throw new IllegalArgumentException("Exception invalid cost : cost cannot less than zero");
		}
		this.cost = cost;
		this.time = time;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList<MenuItems> getItems() {
		return items;
	}
	public void setItems(ArrayList<MenuItems> selectedItems) {
		this.items = selectedItems;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public Date getTime() {
		return time;
	}
	public void setTime(Date date) {
		this.time = date;
	}
	

}


