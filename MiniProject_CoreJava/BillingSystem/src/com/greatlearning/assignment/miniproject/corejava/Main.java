package com.greatlearning.assignment.miniproject.corejava;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Main {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		boolean Order;
		double totalCost = 0;
		Scanner sc = new Scanner(System.in);
		Date time=new Date();
		Date date=new Date();
		
		ZonedDateTime time1 = ZonedDateTime.now();
		DateTimeFormatter f = DateTimeFormatter.ofPattern("F MMM dd HH:mm:ss zzz yyyy");
		String currentTime = time1.format(f);
		
		ArrayList<Bills> bills = new ArrayList<Bills>();
		Bills bill = new Bills();
		ArrayList<MenuItems> items = new ArrayList<>();

		MenuItems i1 = new MenuItems(1, "Rajma Chawal", 2, 150.0);
		MenuItems i2 = new MenuItems(2, "Momos", 2, 190.0);
		MenuItems i3 = new MenuItems(3, "Red Thai Curry", 2, 180.0);
		MenuItems i4 = new MenuItems(4, "Chaap", 2, 190.0);
		MenuItems i5 = new MenuItems(5, "Chilli Potato", 1, 250.0);

		items.add(i1);
		items.add(i2);
		items.add(i3);
		items.add(i4);
		items.add(i5);
    while(true) {
		System.out.println(
				"Welcome to Surabi Restaurant\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

	
			System.out.println("Please Enter the Credentials");

			System.out.println("Email id = ");
			String email = sc.next();

			System.out.println("Password = ");
			String password = sc.next();

			System.out.println("Please Enter A if you are Admin and U if you are User and l to logout ");
			String select = sc.next();
			
			ArrayList<MenuItems> selectedItems = new ArrayList<>();
						
			String name = Character.toUpperCase(password.charAt(0)) + password.substring(1);
			if (select.equalsIgnoreCase("u"))  {

				System.out.println("Welcome  " + name);
				do {
					System.out.println("Today's Menu :- ");
					items.stream().forEach(i -> System.out.println(i));
					System.out.println("Enter the Menu Item Code");
					int itemcode = sc.nextInt();
					
					if (itemcode == 1) {
						selectedItems.add(i1);
						totalCost += i1.getPrice();
					}

					else if (itemcode == 2) {
						selectedItems.add(i2);
						totalCost += i2.getPrice();
					} else if (itemcode == 3) {
						selectedItems.add(i3);
						totalCost += i3.getPrice();
					} else if (itemcode == 4) {
						selectedItems.add(i4);
						totalCost += i4.getPrice();
					} else {
						selectedItems.add(i5);
						totalCost += i5.getPrice();
					}
					
					System.out.println("Press 0 to show bill\nPress 1 to order more");
					int option = sc.nextInt();
					if (option == 0)
						Order = false;
					else
						Order = true;

				} while (Order);
				

				System.out.println("Thanks  " + name + " for dining in with Surabi ");
				System.out.println("Items you have Selected");
				selectedItems.stream().forEach(e -> System.out.println(e));
				System.out.println("Your Total bill will be " + totalCost);
                
				bill.setName(name);
				bill.setCost(totalCost);
				bill.setItems(selectedItems);
				bill.setTime(date);
				bills.add(bill);

			} else if (select.equalsIgnoreCase("a")) {
				System.out.println("Welcome Admin");
				System.out.println(
						"Press 1 to see all the bills for today\nPress 2 to see all the bills for this month\nPress 3 to see all the bills");
				int option = sc.nextInt();
				switch (option) {
				case 1:
					if ( !bills.isEmpty()) {
						for (Bills b : bills) {
							if(b.getTime().getDate()==time.getDate()) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
						}
					} else
						System.out.println("No Bills today.!");
					break;

				case 2:
					if (!bills.isEmpty()) {
						for (Bills b : bills) {
							if (b.getTime().getMonth()==time.getMonth()) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
						}
					} else
						System.out.println("No Bills for this month.!");
					break;

				case 3:
					if (!bills.isEmpty()) {
						for (Bills b : bills) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
					} else
						System.out.println("No Bills.!");

					break;

				default:
					System.out.println("Invalid Option");
					System.exit(1);
				}
			} else if (select.equalsIgnoreCase("l")) {
				System.exit(1);
			}else  {
				System.out.println("Invalid Entry");
				
			}

	}
 }
}




