package com.greatlearning.assignment.miniproject.corejava;

public class MenuItems {
	private int id;
	private String name;
	private int quantity;
	private double price;
	
	
	
	
	public MenuItems() {
		super();
		
	}

	public MenuItems(int id, String name, int quantity, double price) throws IllegalArgumentException{
		super();
		if(id<0)
		{
			throw new IllegalArgumentException("Exception Invalid Id : Id cannot be less than 0");
		}
		this.id = id;
		
		if(name==null) {
			throw new IllegalArgumentException("Exception Invalid Name : Name cannot be null");

		}
		this.name = name;
		if(quantity<0)
		{
			throw new IllegalArgumentException("Exception Invalid Quantity : Quantity cannot be less than 0");
		}
		
		this.quantity = quantity;
		
		if(price<0)
		{
			throw new IllegalArgumentException("Exception Invalid Price : Price cannot be less than 0");
		}
		this.price = price;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "MenuItems [id=" + id + ", name=" + name + ", quantity=" + quantity + ", price=" + price + "]";
	}
	
	


}   
